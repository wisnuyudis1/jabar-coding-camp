//soal 1
function next_date(tanggal,bulan,tahun){
	var temp = new Date(tahun+'-'+bulan+'-'+tanggal);
	var currentDate = new Date(temp.getTime() + 24 * 60 * 60 * 1000);
	var day = currentDate.getDate();
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var bulanKata="";
	switch(month) {
	  case 1:   { bulanKata = 'Januari'; break; }
	  case 2:   { bulanKata = 'Februari'; break; }
	  case 3:   { bulanKata = 'Maret'; break; }
	  case 4:   { bulanKata = 'April'; break; }
	  case 5:   { bulanKata = 'Mei'; break; }
	  case 6:   { bulanKata = 'Juni'; break; }
	  case 7:   { bulanKata = 'Juli'; break; }
	  case 8:   { bulanKata = 'Agustus'; break; }
	  case 9:   { bulanKata = 'September'; break; }
	  case 10:   { bulanKata = 'Oktober'; break; }
	  case 11:   { bulanKata = 'November'; break; }
	  case 12:   { bulanKata = 'Desember'; break; }
	  default:  { bulanKata = 'undefined'; }}

	return day + " " + bulanKata + " " + year ;
}

console.log(next_date(29,2,2020));

// soal 2
function jumlah_kata(kata) { 
  return kata.split(" ").length;
}

var kalimat_1 = "Halo nama saya Wisnu Yudistirawan";
var kalimat_2 = "Saya Ucup Surucup";

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));