// soal 1
var nilai = 54;
var index;

if (nilai >= 85) {
  index = "A";
} else if (nilai >= 75){
  index = "B";
} else if (nilai >= 65){
  index = "C";
} else if (nilai >= 55){
  index = "D";
} else if (nilai < 55){
  index = "E"
}

console.log(index);

// soal 2
var tanggal = 23;
var bulan = 9;
var tahun = 2000;
var bulanKata;

switch(bulan) {
  case 1:   { bulanKata = 'Januari'; break; }
  case 2:   { bulanKata = 'Februari'; break; }
  case 3:   { bulanKata = 'Maret'; break; }
  case 4:   { bulanKata = 'April'; break; }
  case 5:   { bulanKata = 'Mei'; break; }
  case 6:   { bulanKata = 'Juni'; break; }
  case 7:   { bulanKata = 'Juli'; break; }
  case 8:   { bulanKata = 'Agustus'; break; }
  case 9:   { bulanKata = 'September'; break; }
  case 10:   { bulanKata = 'Oktober'; break; }
  case 11:   { bulanKata = 'November'; break; }
  case 12:   { bulanKata = 'Desember'; break; }
  default:  { bulanKata = 'undefined'; }}

console.log(tanggal+' '+bulanKata+' '+tahun);

// soal 3
var n = 5;
var pagar ='';
for(var i = 1; i <= n; i++){
  for(var j = 1; j <= i; j++){
    pagar += '#';
  }
  pagar += '\n';
}
console.log(pagar);


// soal 4
var m = 18;
var h = 0;
var kata;
var samadengan='';
for(var i = 1; i <= m; i++){

  if ((i - h) == 3){
    kata = "I Love VueJS";
    h += 3;
  } else if ((i - h) == 2){
    kata = "I Love Javascript";
  } else if ((i - h) == 1){
    kata = "I Love Programming";
  }

  console.log(i+' - '+kata);

  if ((i%3)===0){
    for (var k = 1; k <= i; k++){
      samadengan += '=';
    }
    console.log(samadengan);
    samadengan = '';
  }
}
