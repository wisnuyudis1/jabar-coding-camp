// soal 1
const luasPersegiPanjang = (p,l) => p * l;
const kelilingPersegiPanjang = (p,l) => 2 * (p+l);

console.log(luasPersegiPanjang(8,5));
console.log(kelilingPersegiPanjang(8,5));

// soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    }
  }
}

newFunction("Wisnu", "Yudistirawan").fullName();

// soal 3
const newObject = {
  firstName: "Wisnu",
  lastName: "Yudistirawan",
  address: "Subang",
  hobby: "Coding",
}

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west,...east];

console.log(combined);

// soal 5
const planet = "earth";
const view = "glass";
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);