// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();
for (var i = 0; i <= daftarHewan.length; i++){
  console.log(daftarHewan[i]);
}

// soal 2
function introduce(data){
  console.log("Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby );
}

var data = {name : "Wisnu" , age : 21 , address : "Subang" , hobby : "Coding" }

var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3
function hitung_huruf_vokal(str) {
  var cek1 = str.match(/[aeiou]/gi).length;
  return cek1;
}

var hitung_1 = hitung_huruf_vokal("Wisnu")
var hitung_2 = hitung_huruf_vokal("Yudistirawan")

console.log(hitung_1 , hitung_2);

// soal 4
function hitung(bilangan){
  return (bilangan * 2) - 2;
}
console.log( hitung(0) );
console.log( hitung(1) );
console.log( hitung(2) );
console.log( hitung(3) );
console.log( hitung(5) );
