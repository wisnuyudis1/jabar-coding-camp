// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var one = pertama.substr(0, 4);
var two = pertama.substr(12, 6);
var three = kedua.substr(0, 7);
var four = kedua.substr(8, 10).toUpperCase();

console.log(one +' '+ two +' '+ three +' '+four);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var hasil = (parseInt(kataKedua) * parseInt(kataKetiga)) + (parseInt(kataPertama) + parseInt(kataKeempat));
console.log(hasil);

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataSatu = kalimat.substring(0, 3);
var kataDua = kalimat.substring(4, 15);
var kataTiga = kalimat.substring(15, 19);
var kataEmpat = kalimat.substring(19, 25);
var kataLima = kalimat.substring(25, 32);

console.log('Kata Pertama: ' + kataSatu);
console.log('Kata Kedua: ' + kataDua);
console.log('Kata Ketiga: ' + kataTiga);
console.log('Kata Keempat: ' + kataEmpat);
console.log('Kata Kelima: ' + kataLima);
